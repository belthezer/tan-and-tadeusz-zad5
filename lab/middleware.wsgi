class middleware:
        def __init__(self, app):
            self.wrapped_app = app

        def __call__(self, environ, start_response):
            for data in self.wrapped_app(environ, start_response):
                yield data.upper()



#!/usr/bin/python
# -*- coding=utf-8 -*-

import urlparse
import sys
#sys.path.insert(0, "/var/www/wsgi-bin")
sys.path.insert(0, "/home/p5/public_html/wsgi-bin")
from bookdb import BookDB
from cgi import escape
#from bookdb import BookDB


body = """
<html>
    <head>
        <meta charset="utf-8" />
        <title>WSGI</title>
        <style>
        a{
        color: #000000;
        }
        a:hover{
        color:#0000ff;
        }
        </style>
    </head>
    <body>
"""

def application(environ, start_response):
    d = urlparse.parse_qs(environ.get('QUERY_STRING', 'Unset'))
    idd = d.get('id', [''])
    host = "194.29.175.240/~p5/wsgi-bin/books.wsgi"
    response_body = body
    
    ctn = False
    for i in BookDB().titles():
        if (i['id'][2:] == idd[0]):
            response_body+="<h2><a href='http://"+host+"'>Back</a></h2>"
            response_body+="<h4>Tytuł: </h4><h3>"+BookDB().title_info('id'+idd[0])['title']+"</h3>"
            response_body+="<h4>ISBN: </h4><h3>"+BookDB().title_info('id'+idd[0])['isbn']+"</h3>"
            response_body+="<h4>Publisher: </h4><h3>"+BookDB().title_info('id'+idd[0])['publisher']+"</h3>"
            response_body+="<h4>Author: </h4><h3>"+BookDB().title_info('id'+idd[0])['author']+"</h3>"
            ctn = True
     
    
    if ctn == False:
        for i in BookDB().titles():
                response_body+="<h3>"+i['id']+": <a href='http://"+host+"/?id="+i['id'][2:]+"'>"+i['title']+"</a></h3>"
     
    response_body+="</body></html>"
    
    status = '200 OK'

    response_headers = [('Content-Type', 'text/html'),
                        ('Content-Length', str(len(response_body)))]
    start_response(status, response_headers)

    return [response_body]


if __name__ == '__main__':
    from wsgiref.simple_server import make_server
    srv = make_server('194.29.175.240', 31005, application)
    srv.serve_forever()
